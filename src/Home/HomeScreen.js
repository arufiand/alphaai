import { View, Text } from 'react-native'
import React, {useEffect, useState} from 'react';
import JSONData from '../../config/interview_question_v1.json';
import DropDownPicker from 'react-native-dropdown-picker';
import { Button } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';



const HomeScreen = ({navigation}) => {
  const [firstInit,setFirstInit] = useState(true)
  const [dataJson, setDataJson] = useState([]);
  const [openBrand, setOpenBrand] = useState(false);
  const [openModel, setOpenModel] = useState(false);
  const [openDamage, setOpenDamage] = useState(false);
  const [brand, setBrand] = useState(null);
  const [brandItem, setBrandItem] = useState([
    // {label: 'Toyota', value: 'Toyota'},
    // {label: 'Mercedez', value: 'Mercedez'}
  ]);
  const [model, setModel] = useState(null);
  const [modelItem, setModelItem] = useState([
  ]);

  const [damage, setDamage] = useState([]);
  const [damageItem, setDamageItem] = useState([
    {label: 'Dent', value: 'Dent'},
    {label: 'Scratch', value: 'Scratch'}
  ]);

  const [detailDamage, setDetailDamage] = useState([]);

  const [modelToyota, setModelToyota] =useState([]);
  const [modelMercy, setModelMercy] =useState([]);

  useEffect(() => {
    setDataJson(JSONData);
    // console.log(JSON.stringify(JSONData.Toyota, null,2));
  }, [firstInit === true]);

  const getBrand = () => {
    setFirstInit(false);
    var brandsGet = []
    var getBrand = JSONData;
    for (var k in getBrand) brandsGet.push({label : k, value : k});
    brandsGet.forEach(element => {
      console.log(element);
      setBrandItem(prevArray => [...prevArray, element]);
    });
  }
 
  const getModelBasedOnBrand = () => {
    Object.keys(JSONData).forEach(function(key,index) {
      if (key == "Toyota") {
        var arrToyota = []
          const lenght = Object.keys(JSONData[key]).length -1;
          console.log(`lenght ${lenght}`);
          // console.log(JSON.stringify(JSONData[key][0]));
          for(let i = 0; i<= lenght; i++){
            var value = {
              id : `Toyota-${JSONData[key][i].Model}-${JSONData[key][i].Damage_Type}`,
              label : JSONData[key][i].Model,
              value : JSONData[key][i].Model,
              damage_type : JSONData[key][i].Damage_Type,
              // Data : JSONData[key][i]
            }
            arrToyota.push(value);
            
          }
          // console.log(JSON.stringify(arrToyota,null,2));
          setModelToyota(prevArray => [...prevArray,arrToyota])

      }
      else {
        var arrMercy = []
          const lenght = Object.keys(JSONData[key]).length -1;
          console.log(`lenght ${lenght}`);
          // console.log(JSON.stringify(JSONData[key][0]));
          for(let i = 0; i<= lenght; i++){
            var value = {
              id : `Mercy-${JSONData[key][i].Model}-${JSONData[key][i].Damage_Type}`,
              label : JSONData[key][i].Model,
              value : JSONData[key][i].Model,
              damage_type : JSONData[key][i].Damage_Type,
              // Data : JSONData[key][i]
            }
            arrMercy.push(value);
            
          }
          // console.log(JSON.stringify(arrMercy,null,2));
          setModelMercy(prevArray => [...prevArray,arrMercy])

      }
      // console.log(JSON.stringify(value,null,2));
    });
  }

// useEffect fot changing the data model
useEffect(() => {
  if(brand == "Toyota"){
    setModelItem([]);
    setModelItem([modelToyota])
  }
  else {
    setModelItem([]);
    setModelItem([modelMercy])
  }
}, [brand])

useEffect(() => {
  console.log(`Isi Model Item ${JSON.stringify(modelItem, null, 2)}`);
}, [modelItem])

  useEffect(() => {
    if(firstInit == true){
      getBrand();
      // console.log(JSON.stringify(brandItem, null, 2));
      getModelBasedOnBrand();
    }
    // console.log(`Toyota Model ${JSON.stringify(modelToyota,null,2)}`);
    // console.log(`Mercy Model ${JSON.stringify(modelMercy,null,2)}`);
    // console.log(`Mercy Model ${JSON.stringify(brandItem,null,2)}`);
  },[firstInit == true])
  
  useEffect(() => {
    // setDetailDamage([
    //   {
    //     "brand" : brand,
    //     "model" : model,
    //     "damage" : 
    //       damage 
    //   }
    // ])
    setDetailDamage([
      {
        "brand" : "Toyota",
        "model" : "RAV4",
        "damage" : [
          {
            "damaged_part":"Front_Side",
            "damage_type" : "Dent",
            "damage_price" : 3000
          },
          {
            "damaged_part":"Front_Door",
            "damage_type" : "Scratch",
            "damage_price" : 2000
          }
        ]
      }
    ])
  }, [damage]);


  //create function for opening dropdown to open one by one

  const sentData = () => {


    console.log(JSON.stringify(detailDamage,null,2));
    navigation.navigate('Counter', {"subDetail" : detailDamage});
    setDetailDamage ([]);
  }
  
  return (
   <View>
    <Text style={{justifyContent:'space-around'}}>
      Alpha AI Testing
    </Text>
   <View style={{padding:20}}>
      <Text>
        Brands
      </Text>
      <DropDownPicker
        open={openBrand}
        value={brand}
        items={brandItem}
        setOpen={setOpenBrand}
        setValue={setBrand}
        setItems={setBrandItem}
        style={{marginBottom:60}}
      />

      <Text>
        Model
      </Text>

      <DropDownPicker
        open={openModel}
        value={model}
        items={modelToyota}
        setOpen={setOpenModel}
        setValue={setModel}
        setItems={setModelItem}
        style={{marginBottom:20}}
      />

      <Text>
        Damage Type
      </Text>
      <DropDownPicker
        multiple={true}
        min={0}
        max={5}
        open={openDamage}
        value={damage}
        items={damageItem}
        setOpen={setOpenDamage}
        setValue={setDamage}
        setItems={setDamageItem}
        style={{marginBottom:50}}
      />

      
    </View>
    <Button style={{padding : 20}} icon="camera" mode="contained" onPress={() => sentData()}>
       Press me
    </Button>
   </View>
  );
}

export default HomeScreen

// https://aboutreact.com/multiple-select-dropdown-picker-example-in-react-native/
// https://www.youtube.com/watch?v=W2zgiwni1uo&ab_channel=ReactNativeLibraries