import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import HomeScreen from '../src/Home/HomeScreen'
import CounterScreen from '../src/Counter/CounterScreen'
const stack = createStackNavigator();

const Router = () => {
    return(
        <stack.Navigator 
            initialRouteName = "Home"
            screenOptions={{
                headerShown:false
            }}>
                <stack.Screen name ="Home" component={ HomeScreen }/>
                <stack.Screen name ="Counter" component={ CounterScreen }/>
        </stack.Navigator>
    )
}

export default Router