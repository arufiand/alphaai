import React, {useEffect,useState} from 'react';
import { View, StyleSheet, FlatList,Text } from 'react-native';
import { DataTable } from 'react-native-paper';


const CounterScreen = ({route,navigation, props}) => {

  // JSON Format 
  // {
  //   damage_type : String,
  //   regions : ["string","string"],
  //   const : interger(total cost from combined regions, damage type and model + brand car)
  // }
  const [initStatus, setInitStatus] = useState(true);
  const [detail, setDetail] = useState('');
  const [count, setCount] = useState(0);
  const [brand, setBrand] = useState('');
  const [model, setModel] = useState('');
  const [damagedPart, setDamagedPart] = useState([]);
  const [cost, setCost] = useState(0);
  const [countDamage, setCountDamage] = useState(0);

  useEffect(() => {
    let index = 0;
    // console.log(JSON.stringify(route.params.subDetail, null,2))
    setDetail(route.params.subDetail);
    if(initStatus == true){
      RenderItem();
    }
  }, []);
  
  // useEffect(() => {
  //   console.log(JSON.stringify(damagedPart,null,2));
  //   // console.log(detail)
  //   // console.log(Object.keys(detail).length);

  // }, [damagedPart])
  
  const RenderItem = () => {
    setInitStatus(false);
    let data = route.params.subDetail;
    var count = 0 
    data.map((item) => {
      console.log(item);
      setBrand(item.brand);
      setModel(item.model);
      item.damage.map((inside) => {
        count =+ count + inside.damage_price 
        setCost(count);
        setDamagedPart(prevArray => [...prevArray,{
          damaged_part : inside.damaged_part,
          damaged_type : inside.damage_type,
          damage_price : inside.damage_price
        }])
      })
      var countDamage = Object.keys(item.damage).length
      console.log(countDamage);
      setCountDamage(countDamage);
    })
  }


  return (
    <View style={styles.container}>
      <Text>
      {countDamage} Damaged Parts Detected
      </Text>
          <DataTable>
            <DataTable.Header>
              <DataTable.Title>No</DataTable.Title>
              <DataTable.Title>Damage Type</DataTable.Title>
              <DataTable.Title>Region</DataTable.Title>
              <DataTable.Title numeric>Costs</DataTable.Title>
            </DataTable.Header>
            {damagedPart.map((item) => {
              console.log(item.damaged_part);
              <DataTable.Row>
                <DataTable.Cell></DataTable.Cell>
                <DataTable.Cell>{item.damaged_type}</DataTable.Cell>
                <DataTable.Cell>{item.damaged_part}</DataTable.Cell>
                <DataTable.Cell numeric>{item.damage_price}</DataTable.Cell>
              </DataTable.Row>
            })}
          
            <Text style={{textAlign:'right',marginTop:30}}>
              Total Estimated Costs $ {cost}
            </Text>
          </DataTable>
        </View>
  )
}

export default CounterScreen
const styles = StyleSheet.create({
  container: {
    paddingTop: 100,
    paddingHorizontal: 30,
  },
});